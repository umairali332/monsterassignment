//
//  AppDelegate.swift
//  MonsterAssignment
//
//  Created by Umair Ali on 4/21/19.
//  Copyright © 2019 Umair Ali. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var appCoordinator : AppCoordinator!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        UIStoryboardSegue.addCoordination()
        uiSettings()
        
        guard let controller = self.window?.rootViewController as? UINavigationController,
            let rootController = controller.topViewController as? PhotoListingViewController else {
                fatalError("No root Coordinated controller")
        }
        self.appCoordinator = AppCoordinator(rootController: rootController)
        self.appCoordinator.start()
        return true
    }
    
    func uiSettings() {
        
//        UIApplication.shared.statusBarStyle = .lightContent
    }


}

