//
//  MediaItemDetailsCoordinator.swift
//  StarzPlayAssignment
//
//  Created by Umair Ali on 4/9/18.
//  Copyright © 2018 Umair Ali. All rights reserved.
//

import UIKit

class MediaDetailsCoordinator : Coordinator {
    
    var rootController : MediaDetailsViewController
    
    init(_ controller : MediaDetailsViewController) {
        
        rootController = controller
    }
    
    func start() {
        
        rootController.coordinationDelegate = self as? CoordinationDelegate
    }
    
    
    
}

extension MediaDetailsViewController : CoordinationDelegate {
    func coordinate(from source: Coordinated, to destination: UIViewController, identifier id: String?) {
        
    }
}
