//
//  AppCoordinator.swift
//  StarzPlayAssignment
//
//  Created by Umair Ali on 4/8/18.
//  Copyright © 2018 Umair Ali. All rights reserved.
//

import UIKit


class AppCoordinator : Coordinator {
    
    fileprivate var rootViewController : PhotoListingViewController
    fileprivate var mediaDetailsCoordinator : MediaDetailsCoordinator!
    fileprivate var selectedItem : FlickerAPIPhotoModel?
    
    init(rootController : PhotoListingViewController) {
        
        rootViewController = rootController
        rootViewController.delegate = self
    }
    
    func start() {
        
        rootViewController.coordinationDelegate = self
    }
}


extension AppCoordinator : CoordinationDelegate {
    
    func coordinate(from source: Coordinated, to destination: UIViewController, identifier id: String?) {
        
        if id == StoryboardSegueIdentifier.showItemDetails {
            
            guard let vc = destination as? MediaDetailsViewController else {
                return
            }
            vc.mediaItem = selectedItem
            mediaDetailsCoordinator = MediaDetailsCoordinator(vc)
            mediaDetailsCoordinator.start()
        }
    }
}

extension AppCoordinator : AppCoordinatorDelegate {
    
    func photoSelected(_ item: FlickerAPIPhotoModel) {
        self.selectedItem = item
        rootViewController.performSegue(withIdentifier: StoryboardSegueIdentifier.showItemDetails, sender: nil)
    }
}
