//
//  MediaDetailsViewController.swift
//  StarzPlayAssignment
//
//  Created by Umair Ali on 4/9/18.
//  Copyright © 2018 Umair Ali. All rights reserved.
//

import UIKit
import AVKit

class MediaDetailsViewController : UIViewController, Coordinated {
    
    weak var coordinationDelegate: CoordinationDelegate?
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    @IBOutlet weak var imageWidth: NSLayoutConstraint!
    
    var mediaItem : FlickerAPIPhotoModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    private func setupView() {
        
        titleLabel.text = mediaItem?.owner
        descriptionLabel.text = mediaItem?.title
        imageWidth.constant = CGFloat(mediaItem?.width ?? 0)
        imageHeight.constant = CGFloat(mediaItem?.height ?? 0)
        posterImageView.lazyLoadImage(withUrlString: mediaItem?.url, placeholderImage: nil)
        
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
