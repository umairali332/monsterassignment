//
//  ListingViewController.swift
//  StarzPlayAssignment
//
//  Created by Umair Ali on 4/8/18.
//  Copyright © 2018 Umair Ali. All rights reserved.
//

import UIKit

protocol AppCoordinatorDelegate : class {
    
    func photoSelected(_ item : FlickerAPIPhotoModel)
}

class PhotoListingViewController : UIViewController , Coordinated {
    
    @IBOutlet weak var activityIndicatorView : UIActivityIndicatorView!
    @IBOutlet weak var collectionView : UICollectionView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    
    weak var coordinationDelegate: CoordinationDelegate?
    weak var delegate : AppCoordinatorDelegate?
    
    private let viewModel = PhotoListingViewModel()
    private var dataSet: Array<FlickerAPIPhotoModel> {
        return viewModel.searchQueryList
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        
        // ViewModel binding
        bindViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    private func bindViewModel() {
        
        viewModel.isDataLoaded.bind {[unowned self] (value) in
            
            self.activityIndicatorView.isHidden = true
            self.collectionView.collectionViewLayout.invalidateLayout()
            self.collectionView.reloadData()
        }
        
        viewModel.errorOccurred.bind { (value) in
            
            self.activityIndicatorView.isHidden = true
        }
    }
    
    private func setupView() {
        collectionView?.backgroundColor = UIColor.clear
        collectionView?.contentInset = UIEdgeInsets(top: 23, left: 4, bottom: 10, right: 4)
        // Set the CustomLayout delegate
        if let layout = collectionView?.collectionViewLayout as? CustomCollectionViewLayout {
            layout.delegate = self
        }
        topViewHeight.constant = UIApplication.shared.statusBarFrame.height
    }
    
    private func startSearch() {
        searchTextField.resignFirstResponder()
        guard let query = searchTextField.text, query.isEmpty == false else {return }
        self.activityIndicatorView.isHidden = false
        viewModel.fetchNewSearchResult(with: query)
    }
    
    private func loadNextPage() {
        guard let query = searchTextField.text, query.isEmpty == false else {return }
        viewModel.fetchNextSearchResult(with: query)
    }
    
    @IBAction func searchButtonTapped(_ sender: Any) {
        startSearch()
    }
    
}

extension PhotoListingViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSet.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoCollectionViewCell.cellIdentifier, for: indexPath)
        
        (cell as? PhotoCollectionViewCell)?.data = dataSet[safe:indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let item = dataSet[safe:indexPath.item] else { return }
        delegate?.photoSelected(item)
    }
    
}

//MARK: - Custom LAYOUT DELEGATE
extension PhotoListingViewController : CustomCollectionViewLayoutDelegate {
    
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath, with width: CGFloat) -> CGFloat {
        let data = self.dataSet[indexPath.item]
        let height = (width/CGFloat(data.width)) * CGFloat(data.height)
        return height
    }
}

extension PhotoListingViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        startSearch()
        return true
    }
}

extension PhotoListingViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if ((collectionView.contentSize.height - collectionView.frame.height) -  collectionView.contentOffset.y) < 200 && !dataSet.isEmpty {
            loadNextPage()
        }
    }
}
