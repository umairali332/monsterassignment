//
//  ListingViewModel.swift
//  StarzPlayAssignment
//
//  Created by Umair Ali on 4/8/18.
//  Copyright © 2018 Umair Ali. All rights reserved.
//

import Foundation

class PhotoListingViewModel : NSObject {
    
    var isDataLoaded = Box<Bool>(false)
    var errorOccurred = Box<String?>(nil)
    var searchQueryList : [FlickerAPIPhotoModel] = []
    
    private var currentPage: Int
    private var isRequestInProgress: Bool = false
    private var dataStore : PhotoDataStore
    private var isMoreDataAvailable: Bool = true
    
    public init( dataStore : PhotoDataStore = PhotoAPIDataStore()) {
    
        self.dataStore = dataStore
        self.currentPage = 1
        super.init()
    }
    
    func fetchNewSearchResult(with query:String) {
        currentPage = 1
        searchQueryList.removeAll()
        loadData(query: query, page: currentPage)
    }
    
    func fetchNextSearchResult(with query:String) {
        loadData(query: query, page: currentPage)
    }
    
    private func loadData(query: String, page: Int) {
        guard isRequestInProgress == false else { return }
        
        let request = FlickerPhotoAPIRequestModel(query: query, page: page)
        self.isRequestInProgress = true
        dataStore.getPhotoList(request: request) {[weak self] (result) in
            
            guard let selfStrong = self else {
                
                return
            }
            DispatchQueue.main.async {
             
                selfStrong.isRequestInProgress = false
                switch result {
                    
                case .success(let data):
                    selfStrong.dataLoaded(data: data)
                case .failure(let error):
                    selfStrong.errorOccurred.value = error.description
                }
            }
        }
    }
    
    private func dataLoaded(data: FlickerAPIResponseModel) {
        self.searchQueryList += data.photo.photoList
        self.currentPage += 1
        isDataLoaded.value = true
    }
}
