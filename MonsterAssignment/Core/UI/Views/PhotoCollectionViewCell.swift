//
//  MediaCollectionViewCell.swift
//  StarzPlayAssignment
//
//  Created by Umair Ali on 4/9/18.
//  Copyright © 2018 Umair Ali. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell : UICollectionViewCell {
    
    @IBOutlet weak var imageView : UIImageView!
    
    static var cellIdentifier : String {
        
        return String(describing: PhotoCollectionViewCell.self)
    }
    
    var data : FlickerAPIPhotoModel? {
        
        didSet{
            configureCell()
        }
    }
    
    private func configureCell() {
        
        imageView.lazyLoadImage(withUrlString: data?.url, placeholderImage: nil)
    }
}
