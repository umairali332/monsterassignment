//
//  MediaDataStore.swift
//  StarzPlayAssignment
//
//  Created by Umair Ali on 4/8/18.
//  Copyright © 2018 Umair Ali. All rights reserved.
//

import Foundation
import AlamofireNetworkLayer

protocol PhotoDataStore {
    
    func getPhotoList( request: FlickerPhotoAPIRequestModel, onCompletion : @escaping ResultHandler<FlickerAPIResponseModel>)
}
