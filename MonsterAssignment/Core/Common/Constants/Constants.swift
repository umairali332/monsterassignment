//
//  Constants.swift
//  StarzPlayAssignment
//
//  Created by Umair Ali on 4/8/18.
//  Copyright © 2018 Umair Ali. All rights reserved.
//

import Foundation

struct APIURLs {
    
    static let baseURL = "https://api.flickr.com"
    
    struct Paths {
        
        static let photoSearch = "/services/rest"
    }
    
}

struct StoryboardSegueIdentifier {
    
    static let showItemDetails = "details"
}

struct APPKeys {
    
    struct APIKeys {
        
        static let flicker = "675894853ae8ec6c242fa4c077bcf4a0"
        
    }
    
}

