//
//  MediaAPIRequestModel.swift
//  StarzPlayAssignment
//
//  Created by Umair Ali on 4/8/18.
//  Copyright © 2018 Umair Ali. All rights reserved.
//

import Foundation

struct FlickerPhotoAPIRequestModel: Codable {
    
    let query:String
    let page:Int
    let apiKey:String = APPKeys.APIKeys.flicker
    let method:String = "flickr.photos.search"
    let extras:String = "url_s"
    let format:String = "json"
    let nojsoncallback:String = "1"
    
    private enum CodingKeys: String, CodingKey {
        case apiKey = "api_key"
        case query = "text"
        case page, method, extras, format, nojsoncallback
    }
    
}
