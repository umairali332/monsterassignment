//
//  MediaAPIDataStore.swift
//  StarzPlayAssignment
//
//  Created by Umair Ali on 4/8/18.
//  Copyright © 2018 Umair Ali. All rights reserved.
//

import Foundation
import AlamofireNetworkLayer

struct PhotoAPIDataStore : PhotoDataStore {
    
    let networking : Networking = AlamofireNetwork.shared
    let translation:TranslationLayer = JSONTranslation()
    
    func getPhotoList(request: FlickerPhotoAPIRequestModel, onCompletion: @escaping ResultHandler<FlickerAPIResponseModel>) {
        guard let requestParams = try? translation.encode(withModel: request) else { onCompletion(.failure(.RequestFailed)); return  }
        
        networking.requestObject(PhotoAPIRouter.Photo.get(parameters: requestParams)) { (response:DataResponseModel<FlickerAPIResponseModel>) in
            
            onCompletion(response.result)
        }
    }
    
    
    
    

}
