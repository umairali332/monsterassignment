//
//  TVSerialResponseModel.swift
//  StarzPlayAssignment
//
//  Created by Umair Ali on 4/8/18.
//  Copyright © 2018 Umair Ali. All rights reserved.
//

import Foundation

struct FlickerAPIPhotoModel: Decodable {
    
    var id:String = ""
    var owner: String = ""
    var title: String = ""
    var url: String = ""
    var height: Int = 0
    var width: Int = 0
    
    private enum CodingKeys: String, CodingKey {
        
        case id, owner, title
        case url = "url_s"
        case height = "height_s"
        case width = "width_s"
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        owner = try container.decode(String.self, forKey: .owner)
        title = try container.decode(String.self, forKey: .title)
        url = try container.decode(String.self, forKey: .url)
        height = Int(try container.decode(String.self, forKey: .height)) ?? 0
        width = Int(try container.decode(String.self, forKey: .width)) ?? 0
    }
}
