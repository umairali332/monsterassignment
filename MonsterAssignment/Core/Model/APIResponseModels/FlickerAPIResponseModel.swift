//
//  MovieResponseModel.swift
//  StarzPlayAssignment
//
//  Created by Umair Ali on 4/8/18.
//  Copyright © 2018 Umair Ali. All rights reserved.
//

import Foundation

struct FlickerAPIResponseModel: Decodable {
    
    var photo: FlickerPhotoAPIResponseModel
    
    private enum CodingKeys: String, CodingKey {
        case photo = "photos"
    }
}

struct FlickerPhotoAPIResponseModel: Decodable {
    
    var page: Int
    var pages: Int
    var photoList:Array<FlickerAPIPhotoModel>
    
    private enum CodingKeys: String, CodingKey {
        case page, pages
        case photoList = "photo"
    }
}
