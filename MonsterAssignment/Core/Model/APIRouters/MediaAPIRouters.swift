//
//  MediaAPIRouters.swift
//  StarzPlayAssignment
//
//  Created by Umair Ali on 4/8/18.
//  Copyright © 2018 Umair Ali. All rights reserved.
//

import Foundation
import AlamofireNetworkLayer

struct PhotoAPIRouter : BaseUrlRouter {
    
    struct Photo : Readable {
        
        var route: String {
            
            get{
                return PhotoAPIRouter.basePath + APIURLs.Paths.photoSearch
            }
            set{
                
            }
        }
    }
}
